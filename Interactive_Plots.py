import json

from bokeh.layouts import row, column
from bokeh.models import ColumnDataSource, CustomJS, TapTool, YearsTicker
from bokeh.plotting import figure, show
import numpy as np

from bokeh.resources import CDN
from bokeh.embed import autoload_static, json_item, components

yearsStr = ['2009', '2010', '2011', '2012', '2013', '2014', '2015', '2016', '2017']
years = []
for year in yearsStr:
    years.append(np.datetime64(year))
years

# Plot 1 --------------------------------------------------------------------------------------------------------------
source_bars = ColumnDataSource({'x': years, 'y': [52953, 53255, 49610, 51515, 51416, 52491, 52305, 51300, 49748]})

plot1 = figure(tools='tap', x_axis_type='datetime', title='Year', plot_width=1132, y_range=(49000, 54000))
plot1.xaxis.ticker = YearsTicker()
bars1 = plot1.vbar(x='x', top='y', source=source_bars, bottom=0, width=10000000000)

# plot 2 --------------------------------------------------------------------------------------------------------------
deprivationCat = [
    '0~20%',
    '20~40%',
    '40~60%',
    '60~80%',
    '80~100%'
]

deprivation = [
    np.array([10756, 10441, 10640, 10855, 10261]),
    np.array([10753, 10314, 10587, 11083, 10518]),
    np.array([9834, 9327, 10105, 10353, 9991]),
    np.array([10368, 9926, 10472, 10707, 10042]),
    np.array([10489, 9851, 10400, 10757, 9919]),
    np.array([10700, 10138, 10646, 11004, 10003]),
    np.array([10866, 10088, 10320, 11050, 9981]),
    np.array([11104, 10138, 10093, 10175, 9790]),
    np.array([10551, 9746, 9695, 10017, 9739])
]

plot2 = figure(x_range=deprivationCat, title='Deprivation', plot_width=1132, y_range=(8000, 12000))
bars2 = plot2.vbar(x='x', top='y', source=ColumnDataSource({'x': deprivationCat, 'y': deprivation[0]}), bottom=0,
                   width=0.5)
bars2.visible = False

# plot 3 --------------------------------------------------------------------------------------------------------------
direction = [
    'Higher Education',
    'Further Education',
    'Training',
    'Employment',
    'Voluntary Work',
    'Activity Agreement',
    'Unemployed seeking',
    'Unemployed Not Seeking',
    'Unknown'
]

totalValues = [
    np.array([18090, 12889, 1732, 12181, 238, 0, 6370, 937, 516]),
    np.array([18306, 13099, 1775, 12689, 283, 304, 5434, 864, 501]),
    np.array([17895, 12320, 1798, 11866, 237, 330, 4000, 881, 283]),
    np.array([19013, 12606, 1590, 12686, 240, 447, 3922, 844, 167]),
    np.array([20057, 13518, 2040, 11162, 226, 539, 3167, 565, 142]),
    np.array([20371, 14511, 1978, 11243, 232, 484, 2815, 582, 275]),
    np.array([21079, 13917, 1355, 11654, 265, 535, 2675, 701, 124]),
    np.array([20879.1, 13748.4, 1231.2, 11286, 307.8, 615.6, 2308.5, 769.5, 205.2]),
    np.array([20471, 13208, 1061, 11272, 339, 601, 1911, 665, 220])
]

plot3 = figure(x_range=direction, title='Directions', plot_width=1132)
bars3 = plot3.vbar(x='x', top='y', source=ColumnDataSource({'x': direction, 'y': totalValues[0]}), bottom=0, width=0.5)
bars3.visible = False

# javascript ----------------------------------------------------------------------------------------------------------
code = '''if (cb_data.source.selected.indices.length > 0){
            bars2.visible = true;
            bars3.visible = true;
            selected_index = cb_data.source.selected.indices[0];
            
            bars2.data_source.data['y'] = deprivation[selected_index]
            bars3.data_source.data['y'] = totalValues[selected_index]
            
            bars2.data_source.change.emit(); 
            bars3.data_source.change.emit(); 
          }'''

plots = column(plot1, plot2, plot3)
plot1.select(TapTool).callback = CustomJS(args={'bars2': bars2,
                                                'bars3': bars3,
                                                'deprivation': deprivation,
                                                'totalValues': totalValues}, code=code)
show(plots)

